//
//  TagApplicable.swift
//  test
//
//  Created by Герман Кононец on 13.01.2020.
//  Copyright © 2020 Герман Кононец. All rights reserved.
//


import UIKit

// MARK: - TagApplicable

/*
 * Все, кто реализует этот протокол, должны иметь возможность отобразить тект
 * с изображениями.
 *
 * Изображения в строке представлены ключами вида `[IC(image_name)]`, где
 * `image_name` является названием изображения или его относительным путем.
 */
public protocol TagApplicable {
    
    /// Задание `attributedText` с изображениями
    func setTextWithTags(_ text: String)
}

public extension TagApplicable where Self: UILabel {
    
    func setTextWithTags(_ text: String) {
        do {
            let regex = try NSRegularExpression(pattern: #"\[IC\([a-z0-9\/]*\)\]"#)
            let results = regex.matches(in: text,
                                            range: NSRange(text.startIndex..., in: text))
            results.forEach {
                let imageName = String(text[Range($0.range, in: text)!])
                let before = text.prefix($0.range.location)
                let afterLength = text.count - (before.count + $0.range.length)
                let after = text.suffix(afterLength)
                attacheImage(imageName: imageName,
                             beforeImage: String(before),
                             afterImage: String(after))
            }
            
        } catch {
            self.text = text
        }
    }
    
    private func getImage(name: String) -> UIImage? {
        let clearName = name.replacingOccurrences(of: "[IC(", with: "").replacingOccurrences(of: ")]", with: "")
        guard let image = UIImage(named: clearName) else {
            return getImageByPath(path: clearName)
        }
        return image
    }
    
    private func getImageByPath(path: String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let userDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths             = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        if let dirPath        = paths.first
        {
           let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(path)
           return UIImage(contentsOfFile: imageURL.path)
        }
        return nil
    }
    
    private func attacheImage(imageName: String, beforeImage: String, afterImage: String){
        let imageAttachment =  NSTextAttachment()
        let completeText = NSMutableAttributedString(string: beforeImage)
        if let image = getImage(name: imageName) {
            imageAttachment.image = image
            let imageOffsetY:CGFloat = -5.0;
            let size = self.font.lineHeight
            imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: size, height: size)
            let attachmentString = NSAttributedString(attachment: imageAttachment)
            completeText.append(attachmentString)
        }
        let  textAfterIcon = NSMutableAttributedString(string: afterImage)
        completeText.append(textAfterIcon)
        self.textAlignment = .center
        self.attributedText = completeText
    }
}

// MARK: - TagApplicable Conformances

extension UILabel: TagApplicable { }

