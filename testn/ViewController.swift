//
//  ViewController.swift
//  testn
//
//  Created by Герман Кононец on 13.01.2020.
//  Copyright © 2020 Герман Кононец. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet private weak var uiLabelOutlet: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        uiLabelOutlet.setTextWithTags("aga[IC(test)], this is test!")
    }


}

